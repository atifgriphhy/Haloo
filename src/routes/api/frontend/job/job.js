const express = require("express");
const router = express.Router();
const generate_otp = require("../../../../utils/generate_otp");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
const Customer = require("../../../../models/user_management/customer");
const SubCategory = require("../../../../models/service_info/sub_category");
const Review = require("../../../../models/review");
const Notification = require("../../../../models/notification/notification");
const Job = require("../../../../models/job");
const moment = require("moment");
const config = require("../../../../../config");
const Agenda = require("agenda");
const agenda = new Agenda({ db: { address: config.MONGODB_URL } });

const timeSince = require("../../../../utils/dateFormatter.js");
// import fcm notification
const sendNotification = require("../fcm/fcm");
// example fcm id

router.put("/rejectjob", async function (req, res) {
  Job.findOne({ _id: "63b8143ff7c2485b026ac0a0" }).then((data) => {
    console.log("job data --->", data);
  });

  let _id = req.query.id;

  _id = ObjectId(_id);
  let rejectType = "customer";
  if ((req.customer.type = "vendor")) {
    rejectType = "vendor";
  }

  if (!_id) {
    return res
      .status(400)
      .send({ error: "Please provide a job id", field: "id" });
  }
  // req.customer._id = ObjectId(req.customer._id);

  let { rejectReason } = req.body;
  let job = await Job.findOne({ _id: _id });

  if (rejectType == "vendor") {
    //   console.log("job---->", job);
    let customerId = job.customerId;
    //   // find socket of this customer
    let customer = Customer.findOne({ _id: customerId });
    let customerSocketId = customer.socketId;
    // send notification to customer
    const registrationToken = customer.registrationToken;

    if (registrationToken) {
      sendNotification(registrationToken, {
        notification: {
          title: "Job Rejected",
          message: "Your job has been rejected by vendor",
          image: job.subCategoryImg,
        },
      });
    }

    // save notification into database
    let notification = new Notification({
      customerId: customerId,
      notification: {
        title: "Job Rejected",
        message: "Your job has been rejected by vendor",
        image: job.subCategoryImg,
      },
    });
    await notification.save();

    //   // send socket request to this customer
    if (customerSocketId) {
      var socket_data = {
        type: "reject",
        data: {
          customerId: customerId,
          vendorId: req.customer._id,
          jobId: _id,
        },
      };
      //     console.log("socket_data", socket_data);
      io.to(customerSocketId).emit("reject", socket_data);
    }
  }

  if (rejectType == "customer") {
    // socket start
    let vendorId = job.vendorId;
    let vendor = await Customer.findOne({ _id: vendorId });
    // send notification to vendor
    const registrationToken = vendor.registrationToken;
    if (registrationToken) {
      sendNotification(registrationToken, {
        notification: {
          title: "Job Rejected",
          message: "Your job has been rejected by customer",
          image: job.subCategoryImg,
        },
      });
    }

    // save notification into database
    let notification = new Notification({
      customerId: vendorId,
      notification: {
        title: "Job Rejected",
        message: "Your job has been rejected by customer",
        image: job.subCategoryImg,
      },
    });
    await notification.save();
    let vendorSocketId = vendor.socketId;

    if (vendorSocketId) {
      var socket_data = {
        type: "reject",
        data: {
          customerId: req.customer._id,
          vendorId: vendorId,
          jobId: _id,
        },
      };
      //     console.log("socket_data", socket_data);
      io.to(vendorSocketId).emit("reject", socket_data);
    }
  }

  Job.findOneAndUpdate(
    {
      _id: _id,
      $or: [{ customerId: req.customer._id }, { vendorId: req.customer._id }],
    },
    { $set: { status: "cancelled", rejectType, rejectReason } },
    { returnOriginal: false, upsert: true }
  )
    .then(function (item) {
      // return new data not ack, and upsert true
      return res.status(200).json({ data: item });
      // res.sendStatus(200);
    })
    .catch((error) => {
      //error handle
      console.log(error);
      res.status(400).send({ error: error });
    });
});
// all the booking
router.get("/", async function (req, res) {
  // sorting by date and filter by live ,completed, cancelled, date
  if (req.query.columnName) {
    delete req.query.columnName;
  }
  if (req.query.sort) {
    delete req.query.sort;
  }

  if (req.query._id) {
    req.query._id = ObjectId(req.query._id);
  }
  req.customer._id = ObjectId(req.customer._id);

  // convert req discount to number
  if (req.query.discount) {
    req.query.discount = Number(req.query.discount);
  }

  try {
    // data = await Job.find(findQuery);

    let data = await Job.aggregate([
      {
        $match: {
          ...req.query,
          $or: [
            { customerId: req.customer._id },
            { vendorId: req.customer._id },
          ],
        },
      },
      {
        $lookup: {
          from: "customers",
          localField: "customerId",
          foreignField: "_id",
          as: "customerDetails",
        },
      },
      {
        $lookup: {
          from: "customers",
          localField: "vendorId",
          foreignField: "_id",
          as: "vendorDetails",
        },
      },
      // {
      //     $lookup: {
      //         from: 'categories ',
      //         localField: 'categoryId',
      //         foreignField: '_id',
      //         as: 'categoryDetails'
      //     }
      // },
      {
        $lookup: {
          from: "subcategories",
          localField: "subCategoryId",
          foreignField: "_id",
          as: "subcategoryDetails",
        },
      },
      // remove the job if status is rejected
      {
        $match: {
          $or: [{ status: { $ne: "rejected" } }],
        },
      },
      // rating of that job to customer
      {
        $lookup: {
          from: "reviews",
          localField: "_id",
          foreignField: "jobId",
          pipeline: [
            {
              $match: {
                reviewFor:
                  req.customer.type === "vendor" ? "customer" : "vendor",
              },
            },
          ],
          as: "reviews",
        },
      },
      {
        $unwind: {
          path: "$reviews",
          preserveNullAndEmptyArrays: true,
        },
      },
      // add rating field and remove reviews
      {
        $addFields: {
          rating: "$reviews.rating",
        },
      },
      // add field avgRating
      {
        $lookup: {
          from: "reviews",
          localField: `${
            req.customer.type === "vendor" ? "customerId" : "vendorId"
          }`,
          foreignField: `${
            req.customer.type === "vendor" ? "customerId" : "vendorId"
          }`,
          as: "allReviews",
        },
      },
      {
        $addFields: {
          avgRating: {
            $round: [{ $avg: "$allReviews.rating" }, 2],
          },
        },
      },

      {
        $project: {
          reviews: 0,
          allReviews: 0,
        },
      },
      {
        $sort: {
          createdAt: -1,
        },
      },
    ]);

    // if loggedIn customer is vendor then hide otp field
    if (req.customer.type === "vendor") {
      data.otp = "";
    }
    // converting creation and updattion time
    // data = data.map((item) => {
    //   item.createdAt = timeSince(item.createdAt);
    //   item.updatedAt = timeSince(item.updatedAt);

    //   return item;
    // });
    res.send({ data: data });
  } catch (error) {
    console.log(error);
    res.status(400).send({ error: error });
  }
});

// add job for vendor
router.post("/", async function (req, res) {
  let _id = req.customer._id;
  let {
    subCategoryId,
    ScheduleTime,
    address,
    distance,
    vendorId,
    latitude,
    longitude,
  } = req.body;

  if (!latitude) {
    return res.status(400).send({ error: "latitude is required" });
  }
  if (!longitude) {
    return res.status(400).send({ error: "longitude is required" });
  }

  if (!distance) {
    return res.status(400).send({ error: "distance is required" });
  }
  if (!address) {
    return res.status(400).send({ error: "address is required" });
  }
  if (!vendorId) {
    return res
      .status(400)
      .send({ error: "Please provide vendor id", field: "vendorId" });
  }

  // check vendor exist
  vendor_details = await Customer.findOne({ _id: vendorId });
  if (!vendor_details) {
    return res
      .status(400)
      .send({ error: "Vendor not found", field: "vendorId" });
  }

  // subCategoryId is id
  if (!mongoose.Types.ObjectId.isValid(req.body.subCategoryId)) {
    return res.status(400).send({
      error: "Please provide subCategory id",
      field: "subCategoryId",
    });
  }

  let otp = generate_otp(4);
  // find subCategory by SubCategoryId and get amount and name
  let subCategory = await SubCategory.findOne({
    _id: subCategoryId,
  });
  let subCategoryImg = subCategory.subCategoryImage;
  let subCategoryName = subCategory.subCategoryName;

  let totalAmount = subCategory.price;
  let finalAmount = subCategory.price;

  let item = new Job({
    subCategoryImg,
    totalAmount,
    finalAmount,
    subCategoryId,
    otp,
    customerId: _id,
    subCategoryName,
    vendorId,
    latitude,
    longitude,
  });

  let customerAvgRating = await Review.find({
    customerId: req.customer._id,
  }).then((data) => {
    let sum = 0;
    data.forEach((element) => {
      sum += element.rating;
    });
    return sum / data.length;
  });

  // find socket of this customer
  let vendor = await Customer.findOne({ _id: vendorId });
  let vendorSocketId = vendor.socketId;
  let registrationToken = vendor.registrationToken;

  var socket_data = {
    type: "job_alert",
    data: {
      customerId: req.customer._id,
      vendorId: vendorId,
      Service: subCategoryName,
      customerName: `${req.customer.firstName} ${req.customer.lastName}`,
      avgRating: customerAvgRating.toFixed(2),
      address: address,
      distance: `${distance} km`,
      latitude: latitude,
      longitude: longitude,
    },
  };

  item
    .save(item)
    .then(function (item) {
      socket_data.data.jobId = item._id.toString();
      socket_data.data.status = item.status;
      // adding createdAt
      socket_data.data.createdAt = item.createdAt;
      // if time is given
      console.log("job created");
      if (ScheduleTime) {
        console.log("job created with schedule time");
        // send socket to vendor at scheduled time
        let futureMs = parseInt(ScheduleTime);
        let nowMs = new Date().getTime();
        let diffMs = futureMs - nowMs;
        let seconds = Math.round(diffMs / 1000);
        console.log("seconds", seconds);

        (async function () {
          agenda.define("send job alert", function (job, done) {
            console.log("send job alert");
            if (registrationToken) {
              sendNotification(registrationToken, {
                notification: {
                  title: "New Job",
                  body: "You have a new job , Please check your dashboard",
                  image: subCategoryImg,
                },
              });
            }
            // save notification in database
            let notification = new Notification({
              customerId: vendorId,
              notification: {
                title: "New Job",
                message: "You have a new job , Please check your dashboard",
                image: subCategoryImg,
              },
            });
            notification.save();
            io.to(vendorSocketId).emit("job_alert", socket_data);
            done();
          });
        })();

        (async function () {
          await agenda.start();
          await agenda.schedule(`in ${seconds} seconds`, "send job alert");
        })();
      } else {
        console.log("job created without schedule time");
        if (registrationToken) {
          sendNotification(registrationToken, {
            notification: {
              title: "New Job",
              body: "You have a new job , Please check your dashboard",
              image: subCategoryImg,
            },
          });
        }
        // save notification in database
        let notification = new Notification({
          customerId: vendorId,
          notification: {
            title: "New Job",
            message: "You have a new job , Please check your dashboard",
            image: subCategoryImg,
          },
        });
        notification.save();
        io.to(vendorSocketId).emit("job_alert", socket_data);
      }

      return res.status(200).send({ data: item });
    })
    .catch((error) => {
      //error handle
      console.log(error);
      return res.status(400).send({ error: error });
    });
});

router.delete("/", async function (req, res) {
  console.log("Got query:", req.query);
  console.log("Got body:", req.body);

  var _id = req.query._id;
  if (!_id) {
    res.status(400).send({ error: "Please provide an id", field: "_id" });
  } else {
    //  remove eleemnt id id mongodb
    Job.deleteOne({
      _id: _id,
      $or: [{ customerId: req.customer._id }, { vendorId: req.customer._id }],
    })
      .then(function (item) {
        return res.status(200).json({ data: item });
      })
      .catch((error) => {
        //error handle
        console.log(error);
        res.status(400).send({ error: error });
      });
  }
});

router.put("/", async function (req, res) {
  console.log("Got query:", req.query);
  console.log("Got body:", req.body);
  let _id = req.query._id;
  console.log("inside update job");

  let { status } = req.body;
  if (!_id) {
    return res
      .status(400)
      .send({ error: "Please provide an job id", field: "id" });
  }

  let job = await Job.findOne({
    _id: _id,
    $or: [{ customerId: req.customer._id }, { vendorId: req.customer._id }],
  });

  if (!job) {
    return res.status(400).send({ error: "Invalid job id" });
  }
  if (status != "ongoing") {
    return res.status(400).send({ error: "Invalid status" });
  }
  if (job.status == "ongoing") {
    return res.status(400).send({ error: "Job is already ongoing" });
  }

  // try catch block with async await to update job
  try {
    let updatedJob = await Job.findOneAndUpdate(
      {
        _id: _id,
        $or: [{ customerId: req.customer._id }, { vendorId: req.customer._id }],
      },
      {
        $set: {
          status: status,
        },
      },
      { new: true }
    );
    // send notification to customer job started
    let customerId = job.customerId;
    let customer = await Customer.findOne({ _id: customerId });
    const registrationToken = customer.registrationToken;
    if (registrationToken) {
      sendNotification(registrationToken, {
        notification: {
          title: "Job Started",
          message: "Your job has been started",
          image: job.subCategoryImg,
        },
      });
    }
    // save notification to database
    let notification = new Notification({
      customerId: customerId,
      notification: {
        title: "Job Started",
        message: "Your job has been started",
        image: job.subCategoryImg,
      },
    });
    await notification.save();

    return res.status(200).send({ data: updatedJob });
  } catch (error) {
    console.log(error);
    return res.status(400).send({ error: error });
  }
});

router.put("/acceptjob", async function (req, res) {
  console.log("Got query:", req.query);
  console.log("Got body:", req.body);
  let _id = req.query.id;

  if (!_id) {
    return res
      .status(400)
      .send({ error: "Please provide an job id", field: "id" });
  }

  //check customer type vendor
  if (req.customer.type != "vendor") {
    return res.status(400).send({ error: "You are not a vendor" });
  }

  // check job.schedule exist
  let job = await Job.findOne({
    _id: _id,
  });
  //  if job is not exist
  if (!job) {
    return res.status(400).send({ error: "Job not found" });
  }

  // if job.schedule exist can not accept job before schedule time
  // if (job?.ScheduleTime) {
  //   // check job.schedule is past
  //   let date = new Date(job.ScheduleTime);
  //   let time = date.getTime();
  //   let currentTime = new Date().getTime();
  //   if (currentTime < time) {
  //     return res.status(400).send({ error: "Job is not scheduled yet" });
  //   }
  // }
  // can accept job 3min after job.schedule time
  // if (job?.ScheduleTime) {
  //   // check job.schedule is past
  //   let date = new Date(job.ScheduleTime);
  //   let time = date.getTime();
  //   let currentTime = new Date().getTime();
  //   //  not allow to accept job after 3min of job.schedule time
  //   if (currentTime > time + 180000) {
  //     return res.status(400).send({ error: "Job is expired" });
  //   }
  // }

  // if (!job?.ScheduleTime) {
  //   // can't accept job after 3min of job created at
  //   console.log("job =====>", job);

  //   let date = new Date(job.createdAt);
  //   let time = date.getTime();
  //   let currentTime = new Date().getTime();
  //   //  not allow to accept job after 3min of job created at
  //   if (currentTime > time + 180000) {
  //     return res.status(400).send({ error: "Job is expired" });
  //   }
  // }

  let customerId = Job.findOne({ _id: _id }).customerId;
  // find socket of this customer
  customerSocketId = Customer.findOne({ _id: customerId }).socketId;
  // send socket request to this customer
  if (customerSocketId) {
    var socket_data = {
      type: "accept",
      data: {
        customerId: customerId,
        vendorId: req.customer._id,
        jobId: _id,
      },
    };
    // console.log("socket_data", socket_data);
    io.to(customerSocketId).emit("accept", socket_data);
  }

  // keep sending current lat , lon to customer after every 2sec
  // let customer = await Customer.findOne({ _id: customerId });
  // let vendor = await Customer.findOne({ _id: vendorId });

  io.on("sendLocation", async (data) => {
    console.log("data", data);
    // 1. lat
    // 2. lon
    // 3. jobId
    // find job by jobId
    const job = await Job.findOne({ _id: data.jobId });
    const customer = await Customer.findOne({ _id: job.customerId });
    const customerId = customer._id;
    const customerSocketId = customer.socketId;
    const registrationToken = customer.registrationToken;
    if (registrationToken) {
      sendNotification(registrationToken, {
        notification: {
          title: "Job Started",
          message: "Your job has been accepted by vendor",
          image: job.subCategoryImg,
        },
      });
    }
    // save notification in db
    const notification = new Notification({
      customerId: customerId,
      notification: {
        title: "Job Started",
        message: "Your job has been accepted by vendor",
        image: job.subCategoryImg,
      },
    });
    await notification.save();

    // send lat lon to customer so that customer can track vendor
    // keep sending lat lon to customer after every 2se
    // setInterval(() => {
    //   io.to(customerSocketId).emit("sendLocation", data);
    // }, 2000);
    io.to(customerSocketId).emit("sendLocation", data);
    // socket.broadcast.to(socketid).emit("message", "for your eyes only");
  });

  Job.findOneAndUpdate(
    { _id: _id, vendorId: req.customer._id },
    { $set: { status: "upcoming" } },
    { returnOriginal: false, upsert: true }
  )
    .then(function (item) {
      // res.sendStatus(200);
      return res.status(200).json({ data: item });
    })
    .catch((error) => {
      //error handle
      console.log(error);
      res.status(400).send({ error: error });
    });
});
router.post("/verify_otp", async (req, res) => {
  console.log("Got query:", req.query);
  console.log("Got body:", req.body);
  if (!req.customer._id) {
    return res.status(400).send("Unable to get id from token please relogin");
  }

  var _id = req.query._id;
  var otp = req.body.otp;

  if (!_id) {
    return res.send({ error: "Please provide an id of the job in params" });
  }
  if (!otp) {
    return res.send({ error: "Please provide an otp in body" });
  }

  let job = await Job.findById(_id);

  if (!job) {
    return res.send({ error: "No job found with this id" });
  }
  if (otp != job.otp) {
    return res.send({ error: "Invalid OTP" });
  }
  if (job.status === "completed") {
    return res.send({ error: "Job is already completed" });
  }
  if (job.status != "ongoing") {
    return res.send({ error: "Job is not in ongoing status" });
  }

  job.status = "completed";
  await job.save();
  // return updated job data
  // send socket request to customer for making payment
  // send socket request to custome to make
  let customerId = job.customerId;
  let customer = await Customer.findOne({
    _id: customerId,
  });
  // find socket of this customer
  let customerSocketId = customer.socketId;
  if (customerSocketId) {
    var socket_data = {
      type: "payment",
      data: {
        customerId: customerId,
        vendorId: req.customer._id,
        _id: _id,
      },
    };
    console.log("socket_data", socket_data);
    io.to(customerSocketId).emit("payment", socket_data);
  }

  // send notification to customer for making payment
  const registrationToken = customer.registrationToken;
  if (registrationToken) {
    sendNotification(registrationToken, {
      notification: {
        title: "Job Completed Successfully",
        message: "Please make payment for your job",
        image: job.subCategoryImg,
      },
    });
  }
  // save notification in db
  const notification = new Notification({
    customerId: customerId,
    notification: {
      title: "Job Completed Successfully",
      message: "Please make payment for your job",
      image: job.subCategoryImg,
    },
  });
  await notification.save();
  // increment noOfCompletedService in vendor
  let vendorId = job.vendorId;
  await Customer.findOneAndUpdate(
    {
      _id: vendorId,
    },
    {
      $inc: { noOfCompletedService: 1 },
    }
  );

  return res.send({ data: job });
});

module.exports = router;
