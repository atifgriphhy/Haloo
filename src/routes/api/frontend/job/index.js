// const express = require("express");
// const router = express.Router();

// module.exports = function () {
//   router.use("/", require("./job")());
//   return router;
// };

const express = require("express");
const router = express.Router();

module.exports = function () {
  // router.use("/", require("./notification"));
  router.use("/", require("./job"));

  return router;
};
