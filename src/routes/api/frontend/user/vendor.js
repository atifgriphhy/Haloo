const express = require("express");
const router = express.Router();
const Customer = require("../../../../models/user_management/customer");
const fs = require("fs");
const upload = require("../../../../middleware/multer");
const mongoose = require("mongoose");
const ObjectId = mongoose.Types.ObjectId;
const SubCategory = require("../../../../models/service_info/sub_category");
const Job = require("../../../../models/job");

// function to calculate distance between two points in km's in javascript
//This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
  var R = 6371; // km
  var dLat = toRad(lat2 - lat1);
  var dLon = toRad(lon2 - lon1);
  var lat1 = toRad(lat1);
  var lat2 = toRad(lat2);

  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c;
  return d;
}

// Converts numeric degrees to radians
function toRad(Value) {
  return (Value * Math.PI) / 180;
}

router.get("/", async (req, res) => {
  console.log("list is called");
  console.log("Got query:", req.query);

  let { columnName, sort, lat, lng } = req.query;
  // delete lat and lng from query
  delete req.query.lat;
  delete req.query.lng;
  if (columnName === "distance") {
    columnName = "nearestDistance";
  }

  console.log("Got columnName:", columnName);
  if (sort) {
    sort = parseInt(sort);
  }
  if (columnName === "" || columnName === undefined || columnName === null) {
    columnName = "createdAt";
  }
  if (sort === "" || sort === undefined || sort === null) {
    sort = -1;
  }

  // delete after taking value of col and sort from query
  if (req.query.columnName) {
    delete req.query.columnName;
  }
  if (req.query.sort) {
    delete req.query.sort;
  }
  if (req.query.online) {
    var online = req.query.online;
    if (online != "true" && online != "false") {
      return res
        .status(400)
        .send({ error: "online must be bool true/false", field: "online" });
    }
    if (online == "true") {
      req.query.online = true;
    } else {
      req.query.online = false;
    }
  }

  console.log("Got query:", req.query);
  const sortMethod = {
    columnName: sort,
  };

  try {
    if (req.query.jobSkillId) {
      req.query.jobSkills = { $in: [new ObjectId(req.query.jobSkillId)] };
      delete req.query.jobSkillId;
    }

    if (req.query._id) {
      req.query._id = new ObjectId(req.query._id);
    }
    data = await Customer.aggregate([
      {
        $match: { ...req.query, type: "vendor" },
      },

      // {
      //   $addFields: { address: "$address" },
      // },
      {
        $lookup: {
          from: "states",
          localField: "address.stateId",
          foreignField: "_id",
          as: "stateDetails",
        },
      },
      {
        $lookup: {
          from: "cities",
          localField: "address.cityId",
          foreignField: "_id",
          as: "cityDetails",
        },
      },

      {
        $lookup: {
          from: "jobs",
          localField: "_id",
          foreignField: "vendorId",
          as: "jobDetails",
        },
      },

      {
        $addFields: {
          noOfJobs: { $size: "$jobDetails" },
        },
      },
      {
        $addFields: {
          noOfCompletedService: {
            $size: {
              $filter: {
                input: "$jobDetails",
                as: "job",
                cond: { $eq: ["$$job.status", "completed"] },
              },
            },
          },
        },
      },
      {
        $addFields: {
          noOfCancelledService: {
            $size: {
              $filter: {
                input: "$jobDetails",
                as: "job",
                cond: { $eq: ["$$job.status", "cancelled"] },
              },
            },
          },
        },
      },
      {
        $project: { jobDetails: 0, reviewDetails: 0, token: 0 },
      },
      {
        $lookup: {
          from: "reviews",
          localField: "_id",
          foreignField: "vendorId",
          as: "reviewDetails",
        },
      },
      {
        $lookup: {
          from: "subcategories",
          localField: "jobSkills",
          foreignField: "_id",
          as: "jobSkills",
        },
      },

      {
        $addFields: {
          AllRating: {
            $map: {
              input: "$reviewDetails",
              as: "review",
              in: {
                rating: "$$review.rating",
              },
            },
          },
        },
      },
      // loop over allrating.rating and calculate average rating
      {
        $addFields: {
          avgRating: {
            $avg: "$AllRating.rating",
          },
        },
      },
      // show average rating upto 2 decimal place
      {
        $addFields: {
          avgRating: {
            $round: ["$avgRating", 2],
          },
        },
      },
      // if avgRating is null then set it to 0
      {
        $sort: sortMethod,
      },
      // remove stateId and cityId from address
      {
        $project: {
          "address.stateId": 0,
          "address.cityId": 0,
        },
      },
    ]);
    // loop through each vendor address and remove address which is not have lat and lng
    // data = data.slice(0, 5);
    if (lat && lng) {
      data = await Promise.all(
        data.map((singleData) => {
          singleData.address.map((singleAddress) => {
            let distance = getDistanceFromLatLonInKm(
              lat,
              lng,
              singleAddress.lat,
              singleAddress.lng
            );
            singleAddress.distance = Math.round(distance);

            return singleAddress;
          });

          return singleData;
        })
      );

      //  map and return nearest address of vendor using math.min.apply
      data = await Promise.all(
        data.map((singleData) => {
          let distanceArray = singleData.address.map(
            (singleAddress) => singleAddress.distance
          );
          let nearestDistance = Math.min.apply(Math, distanceArray);
          singleData.distance = nearestDistance;

          return singleData;
        })
      );
    }
    // uncomment this line to filter data which is less than 5 km
    // data = data.filter((singleData) => singleData.distance <= 5);

    res.status(200).send({ data: data });
  } catch (error) {
    console.log(error);
    res.status(400).send({ error: error });
  }
});

module.exports = router;
