const express = require("express");
const router = express.Router();
const admin = require("firebase-admin");
const serviceAccount = require("./serviceAccountKey.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://sample-project-e1a84.firebaseio.com",
});

const notification_options = {
  priority: "high",
  timeToLive: 60 * 60 * 24,
};
function sendNotification(registrationToken, payLoad) {
  admin
    .messaging()
    .sendToDevice(registrationToken, payLoad)
    .then((response) => {
      console.log("Successfully sent notification:", response);
      return response;
    })
    .catch((error) => {
      console.log("Error sending notification:", error);
      return error;
    });
}

module.exports = sendNotification;
