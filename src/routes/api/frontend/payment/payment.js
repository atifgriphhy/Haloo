const express = require("express");
const router = express.Router();
const Order = require("../../../../models/order/order");
const Customer = require("../../../../models/user_management/customer");
const Razorpay = require("razorpay");
const mongoose = require("mongoose");
const Job = require("../../../../models/job");
const SubCategory = require("../../../../models/service_info/sub_category");
const Offer = require("../../../../models/offer/offer");
const crypto = require("crypto");
// require sendNotification function from fcm.js
const sendNotification = require("../../../../routes/api/frontend/fcm/fcm");
const Notification = require("../../../../models/notification/notification");

// This razorpayInstance will be used to
// access any resource from razorpay
const razorpayInstance = new Razorpay({
  // Replace with your key_id
  key_id: "rzp_test_rsZhchl2dCY4yw",
  key_secret: "tZZZb25tLMos5DoWnXaIQEUI",
});
// createOrder in both cases with cod and online
router.post("/createOrder", async (req, res) => {
  // note to come back here
  // save razorpay order id in jobid
  // rrazorPayOrderId
  let { jobId, couponId, paymentType } = req.body;
  let discountAmount;
  let finalAmount;

  if (!jobId) {
    return res.status(400).json({
      message: "jobId is required",
      field: "jobId",
    });
  }
  let alreadyOrder = await Order.findOne({ jobId: jobId });
  if (alreadyOrder) {
    return res.status(400).json({
      message: "Order is already created",
    });
  }

  if (!paymentType) {
    return res.status(400).json({
      message: "paymentType is required",
      field: "paymentType",
    });
  }

  // check if order_id is and valid id or not
  if (!mongoose.Types.ObjectId.isValid(jobId)) {
    return res.status(400).json({
      message: "subCategoryId is not valid",
      field: "subCategoryId",
    });
  }
  let job = await Job.findOne({ _id: jobId }).populate("subCategoryId");
  // job
  let subCategory = job.subCategoryId;
  if (!job) {
    return res.status(400).json({
      message: "job is not exist",
    });
  }
  if (job.status != "completed") {
    return res.status(400).json({
      message: "Order Can't be created before job is completed",
    });
  }
  if (job.paymentStatus == "completed") {
    return res.status(400).json({
      message: "Payment is already completed",
    });
  }

  if (!couponId) {
    finalAmount = subCategory.price;
  }
  let amount;
  const currency = "INR";
  if (couponId) {
    // verfiy discount available by this discount coupon
    let offer = await Offer.findOne({ code: couponId });

    if (!offer) {
      return res.status(400).send({ error: "Invalid coupon", field: "coupon" });
    }
    console.log("offer", offer);
    let totalAmount = subCategory.price;
    const discount = offer.discount;
    discountAmount = Math.round(discount);
    finalAmount = totalAmount - discountAmount;
    finalAmount = Math.round(finalAmount);
    amount = finalAmount * 100;
  } else {
    amount = (await subCategory.price) * 100;
  }

  if (paymentType == "cod") {
    let order = await Order.create({
      paymentType: "cod",
      paymentStatus: "pending",
      jobId: jobId,
      total: subCategory.price,
      discount: discountAmount,
      finalAmount: finalAmount,
      couponId: couponId,
      currency: "INR",
      customerId: job.customerId,
      vendorId: job.vendorId,
    });
    order = await order.save();

    // find vendorId
    let vendorId = job.vendorId;
    let vendor = await Customer.findOne({ _id: vendorId });
    vendorSocketId = vendor.socketId;

    //  send notification to vendor to confirm payment
    //TODO: Remove this comment for sending notification to vendor

    // let registrationToken = vendor.registrationToken;
    // sendNotification(registrationToken, {
    //   notification: {
    //     title: "Payment Confirmation for cod",
    //     body: "Please confirm payment if you have received payment in cash",
    //   },
    // });
    // send socket request to vendor to confirm payment

    io.to(vendorSocketId).emit("payment", {
      type: "confirmPayment",
      orderId: order._id,
    });

    // save order in database
    order.razorpayOrderId = undefined;
    order.razorpayPaymentId = undefined;
    job.orderId = order._id;
    await job.save();
    return res.status(200).json(order);
  }

  razorpayInstance.orders.create({ amount, currency }, (err, order) => {
    if (!err) {
      let newOrder = new Order({
        paymentType: "online",
        paymentStatus: "pending",
        razorpayOrderId: order.id,
        jobId: jobId,
        total: subCategory.price,
        discount: discountAmount,
        finalAmount: amount / 100,
        couponId: couponId,
        currency: order.currency,
        customerId: job.customerId,
        vendorId: job.vendorId,
      });
      newOrder.save();

      // update orderId in job table
      job.orderId = newOrder._id;
      job.save();
      return res.json(newOrder);
    } else res.send(err);
  });
});
// get Orders
router.get("/order", async (req, res) => {
  let { jobId } = req.query;
  if (!jobId) {
    return res.status(400).json({
      message: "jobId is required",
      field: "jobId",
    });
  }
  // check if _id is and valid id or not
  if (!mongoose.Types.ObjectId.isValid(jobId)) {
    return res.status(400).json({
      message: "jobId is not valid",
      field: "jobId",
    });
  }
  let order = await Order.findOne({ jobId: jobId });

  console.log("orders recieved", order);

  if (!order) {
    return res.status(400).json({
      message: "Order is not exist",
    });
  }
  return res.status(200).json(order);
});
router.post("/verifyOrder", async (req, res) => {
  // STEP 7: Receive Payment Data
  const { order_id, payment_id, jobId } = req.body;

  // check if order_id is is provided or not
  if (!order_id) {
    return res.status(400).json({
      message: "order_id is required",
      field: "order_id",
    });
  }
  // check if payment_id is provided or not
  if (!payment_id) {
    return res.status(400).json({
      message: "payment_id is required",
      field: "payment_id",
    });
  }
  // check if jobId is provided or not
  if (!jobId) {
    return res.status(400).json({
      message: "jobId is required",
      field: "jobId",
    });
  }

  const razorpay_signature = req.body.razorpay_signature;

  if (!razorpay_signature) {
    return res.status(400).json({
      message: "razorpay_signature is required",
      field: "razorpay_signature",
    });
  }
  // Pass yours key_secret here
  const key_secret = "tZZZb25tLMos5DoWnXaIQEUI";

  // STEP 8: Verification & Send Response to User

  // Creating hmac object
  let hmac = crypto.createHmac("sha256", key_secret);

  // Passing the data to be hashed
  hmac.update(order_id + "|" + payment_id);

  // Creating the hmac in the required format
  const generated_signature = hmac.digest("hex");
  console.log("generated_signature ====>", generated_signature);
  console.log("razorpay_signature ====>", razorpay_signature);
  if (razorpay_signature === generated_signature) {
    // update job status
    await Job.findOneAndUpdate(
      { _id: jobId },
      { $set: { paymentStatus: "completed" } }
    );

    let job = await Job.findOne({
      _id: jobId,
    });
    // 2. find vendor by job.vendor
    let vendor = await Customer.findOne({ _id: job?.vendorId });
    if (!vendor) {
      return res.status(400).json({
        message: "vendor is not exist",
      });
    }
    let vendorSocketId = vendor?.socketId;
    if (!vendorSocketId) {
      return res.status(400).json({
        message: "vendorSocketId is not exist",
      });
    }
    // find socket of this customer
    if (vendorSocketId) {
      var socket_data = {
        type: "PaymentSuccessful",
        data: {
          razorPayOrderId: order_id,
        },
      };
      console.log("socket_data", socket_data);
      io.to(vendorSocketId).emit("payment", socket_data);
    }
    // update order status
    await Order.findOneAndUpdate(
      { razorpayOrderId: order_id },
      { $set: { paymentStatus: "completed", razorpayOrderId: order_id } }
    );
    // let job = await Job.findById(jobId);
    const registrationToken = vendor?.registrationToken;
    if (registrationToken) {
      sendNotification(registrationToken, {
        notification: {
          title: "Payment Successful",
          message: "Online payment , has been made by customer",
          image: job.subCategoryImg,
        },
      });
    }
    // save notification in db
    let newNotification = new Notification({
      customerId: job.customerId,
      notification: {
        title: "Payment Successful",
        message: "Online payment , has been made by customer",
        image: job.subCategoryImg,
      },
    });
    await newNotification.save();

    return res.json({ success: true, message: "Payment has been verified" });
  } else {
    return res.json({ success: false, message: "Payment verification failed" });
  }
});

// cod payment
router.post("/confirmCod", async (req, res) => {
  const { orderId } = req.body;
  if (!orderId) {
    return res.status(400).json({
      message: "orderId is required",
      field: "orderId",
    });
  }

  let order = await Order.findById(orderId);
  // check if order is exist or not
  if (!order) {
    return res.status(400).json({
      message: "Order is not exist",
      field: "orderId",
    });
  }

  if (order.vendorId.toString() != req.customer._id.toString()) {
    return res.status(400).json({
      message: "You are not vendor of this order",
      field: "orderId",
    });
  }
  // check if order paymentType is cod or not
  if (order.paymentType != "cod") {
    return res.status(400).json({
      message: "This order is not cod order",
      field: "orderId",
    });
  }

  // check if order is already completed or not
  if (order.paymentStatus == "completed") {
    return res.status(400).json({
      message: "This order is already completed",
      field: "orderId",
    });
  }
  // update order status
  await Order.findOneAndUpdate(
    { _id: orderId },
    { $set: { paymentStatus: "completed" } }
  );
  // update job status
  await Job.findOneAndUpdate(
    { _id: order.jobId },
    { $set: { paymentStatus: "completed" } }
  );

  let customerId = order.customerId;
  let customer = await Customer.findById(customerId);
  let customerSocketId = customer.socketId;

  // find socket of this customer
  // send socket request to this customer
  if (customerSocketId) {
    var socket_data = {
      type: "PaymentSuccessful",
      data: {
        razorPayOrderId: order.razorpayOrderId,
        _id: order._id,
      },
    };
    console.log("socket_data", socket_data);
    io.to(customerSocketId).emit("payment", socket_data);
  }

  // send socket to vendor that payment is successful
  return res.json({ message: "Payment confirmed successfully" });
});
// check if jobId is provided or not
module.exports = router;
