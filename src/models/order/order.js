var mongoose = require("mongoose");
// user,total, Discount(discountAmount), finalAmount

const orderSchema = new mongoose.Schema(
  {
    total: {
      type: Number,
    },
    discount: {
      type: Number,
      default: 0,
    },
    finalAmount: {
      type: Number,
    },

    couponId: {
      type: String,
      default: null,
    },
    jobId: {
      type: String,
      default: null,
    },
    razorpayOrderId: {
      type: String,
      default: null,
    },
    paymentType: {
      type: String,
      enum: ["online", "cod"],
    },
    paymentStatus: {
      type: String,
      enum: ["pending", "completed", "cancelled"],
      default: "pending",
    },
    currency: {
      type: String,
      default: "INR",
    },
    customerId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Customer",
    },
    vendorId: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Customer",
    },
    razorpayPaymentId: {
      type: String,
      default: null,
    },
  },
  {
    timestamps: true,
  }
);

const Order = mongoose.model("order", orderSchema);
module.exports = Order;
