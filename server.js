require("dotenv").config();

const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
const config = require("./config");
const Contactus = require("./src/models/contactus");
const Job = require("./src/models/job");
const Customer = require("./src/models/user_management/customer");
var multer = require("multer");
const cloudinary = require("cloudinary");

const useragent = require("express-useragent");
//creating express intances
const app = express();
// cloudinary
cloudinary.config({
  cloud_name: "dvpcv7poi",
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
  secure: true,
});

app.use(cors());

// log all requests to the console
app.use(function (req, res, next) {
  next();
});

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());

app.use(useragent.express());
app.use("/uploads", express.static("./uploads"));
// mogodb connection
mongoose.set("strictQuery", false);

mongoose.connect(config.MONGODB_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});
const db = mongoose.connection;
// db use with strict query

db.on("error", console.error.bind(console, "Connection error"));
db.once("open", function () {
  console.log("Connection succeeded.");
});
const router = require("./src/routes")();
app.use(router);

const port = process.env.PORT || 8000;
// create http server and run socket io
const server = app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
// run socket io
const io = require("socket.io")(server, {
  cors: {
    origin: "*",
    methods: ["GET", "POST", "PUT", "DELETE"],
  },
});

// listen to connection
io.on("connect", (socket) => {
  console.log("New client connected");
  console.log("socket id", socket.id);
  // save socket id of user to mongodb
  socket.on("save-socket-id", (data) => {
    console.log("save-socket-id called", data);
    console.log("data.customerId", data.customerId);

    // receive customerId and socketId
    // save socketId to in Customer collection
    Customer.findOneAndUpdate(
      { _id: data.customerId },
      { $set: { socketId: data.socketId } },
      { new: true },
      (err, customer) => {
        if (err) {
          console.log(err);
        } else {
          console.log("customer after socket saving---->", customer);
          // reply back to same socket
          socket.emit("save-socket-id", {
            socketId: customer.socketId,
          });
        }
      }
    );
  });
  socket.on("message", (data) => {
    io.emit("message", "You are connected to server successfully");
  });
  // receive lat lon and jobId from customer using socket.io
  socket.on("sendLocation", async (data) => {
    // 1. lat
    // 2. lon
    // 3. jobId
    // find job by jobId
    const job = await Job.findOne({ _id: data.jobId });
    const customer = await Customer.findOne({ _id: job.customerId });
    const customerSocketId = customer.socketId;
    // send lat lon to customer using socket.io
    // socket.broadcast.to(customerSocketId).emit("sendLocation", data);
    // send location only one customer whose socket id is customerSocketId every 2 seconds
    io.to(customerSocketId).emit("sendLocation", data);

    // setInterval(() => {
    //   io.to(customerSocketId).emit("sendLocation", data);
    // }, 2000);
  });
});

global.io = io;
